package com.biom4st3r.netherportals;

import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import net.minecraft.nbt.ByteTag;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;

public class PortalListv2 {

    public static class PortalListEntry {
        public BlockPos.Mutable pos;
        public String name;
        public RegistryKey<World> dim;
        public boolean isPublic;
        public PortalListEntry(long pos, String name, RegistryKey<World> dim, boolean isPublic) {
            this.reassign(new BlockPos.Mutable(), name, dim, isPublic);
            this.pos.set(pos);
        }

        public PortalListEntry() {

        }

        public PortalListEntry reassign(BlockPos pos, String name, RegistryKey<World> dim, boolean isPublic) {
            this.pos = pos.mutableCopy();
            this.name = name;
            this.dim = dim;
            this.isPublic = isPublic;
            return this;
        }

        public PortalListEntry reassign(long pos, String name, RegistryKey<World> dim, boolean isPublic) {
            this.pos.set(pos);
            this.name = name;
            this.dim = dim;
            this.isPublic = isPublic;
            return this;
        }
    }

    public static int DEFAULT_SIZE = 8;

    /**
     * Because this can be accessed by the MAIN thread and NETTIO thread it must be
     * locked during iteration and reinitilization
     */
    private ReentrantLock LOCK = new ReentrantLock();

    public int getSignature() {
        return this.index + this.poses.length + this.names.hashCode();
    }

    private long[] poses;
    private String[] names;
    private RegistryKey<World>[] dims;
    private boolean[] isPublic;
    
    private int index = 0;

    @SuppressWarnings({"unchecked"})
    public PortalListv2(int defaultSize) {
        this.poses = new long[defaultSize];
        this.names = new String[defaultSize];
        this.dims = new RegistryKey[defaultSize];
        this.isPublic = new boolean[defaultSize];
    }

    public PortalListv2() {
        this(DEFAULT_SIZE);
    }

    public boolean isEmpty() {
        return names.length == 0 || names[0] == null;
    }

    /**
     * removed null entries and shrinks contained arrays.
     * Check back to front until it finds a not null value and resizes down to that point
     */
    public void trim() {
        if(this.names.length > this.index) {
            this.resize(this.index - this.names.length);
        }
    }

    /**
     * Reinits arrays to 0
     */
    public void clear() {
        this.reinit(0);
    }

    private void locker(Runnable r) {
        this.LOCK.lock();
        r.run();
        this.LOCK.unlock();
    }

    /**
     * Reassigns contained arrays to {@code size} and resets index
     * 
     * @param size
     */
    @SuppressWarnings({"unchecked"})
    public void reinit(int size) {
        this.locker(()-> {
            this.poses = new long[size];
            this.names = new String[size];
            this.dims = new RegistryKey[size];
            this.isPublic = new boolean[size];
            this.index = 0;
        });
    }

    /**
     * Uses world to collection all important information from {@code pos}
     * 
     * @param world
     * @param referencePoint
     *                           point offset from the start of the area to be
     *                           contained in this BlockContainer
     * @param pos
     */
    public final void add(BlockPos pos, String name, RegistryKey<World> dim, boolean isPublic) {
        this.locker(()-> {
            this.checkSize();
            this.poses[index] = pos.asLong();
            this.names[index] = name;
            this.dims[index] = dim;
            this.isPublic[index] = isPublic;
            this.index++;
        });
    }

    public final void add(PortalListEntry entry) {
        this.add(entry.pos, entry.name, entry.dim, entry.isPublic);
    }

    protected final void checkSize() {
        if (this.index == names.length) {
            this.resize(DEFAULT_SIZE);
        }
    }

    /**
     * Returns a BlockInstance of the information at index.
     * <p>
     * If you need to get a block at a BlockPos use
     * {@code BlockContainer#asBlockRenderView()}
     * 
     * @param i
     * @return
     */
    public PortalListEntry get(int i) {
        if (i >= index) {
            return null;
        }
        return new PortalListEntry(this.poses[i], this.names[i], this.dims[i], this.isPublic[i]);
    }

    public PortalListEntry getLatest()
    {
        return this.get(index-1);
    }

    public void revokeLatest()
    {
        this.index -= 1;
    }

    protected void resize(int expansion) {
        int newSize = names.length + expansion;
        // locker(()-> { COW?
        this.names = Arrays.copyOf(names, newSize);
        this.dims = Arrays.copyOf(dims, newSize);
        this.poses = Arrays.copyOf(poses, newSize);
        this.isPublic = Arrays.copyOf(isPublic, newSize);
        // });
    }

    public PortalListv2 getCopy()
    {
        PortalListv2 c = new PortalListv2();
        c.names = Arrays.copyOf(this.names, this.names.length);
        c.poses = Arrays.copyOf(this.poses, this.poses.length);
        c.dims = Arrays.copyOf(this.dims, this.dims.length);
        c.isPublic = Arrays.copyOf(this.isPublic, this.isPublic.length);
        c.index = this.index;
        c.clone = this.clone + 1;
        return c;
    }

    int clone = 0;

    /**
     * Alt option to using {@code BlockContainer#iterator()}
     * 
     * @param consumer
     * @param world
     */
    public void forEach(Consumer<PortalListEntry> consumer) {
        PortalListEntry instance = new PortalListEntry(0, "", null, false);
        this.locker(()-> {
            for (int i = 0; i < index; i++) {
                instance.reassign(this.poses[i], this.names[i], this.dims[i], this.isPublic[i]);
                consumer.accept(instance);
            }
        });
    }
    /** CONST FOR SERIALIZATION */
    public static final String
        NAMES = "names",
        LENGTH = "length",
        POSITIONS = "poses",
        DIMENSIONS = "dims",
        PUBLICS = "pubs";

    public CompoundTag toTag(CompoundTag tag) {
        this.trim();
        tag.putInt(LENGTH, this.index);

        tag.putLongArray(POSITIONS, Arrays.copyOf(poses, this.index));
        ListTag lt = new ListTag();
        for (int i = 0; i < this.index; i++) {
            lt.add(StringTag.of(this.names[i]));
        }
        tag.put(NAMES, lt);

        lt = new ListTag();
        for (int i = 0; i < this.index; i++) {
            lt.add(StringTag.of(dims[i].getValue().toString()));
        }
        tag.put(DIMENSIONS, lt);

        lt = new ListTag();
        for (int i = 0; i < this.index; i++) {
            lt.add(this.isPublic[i] ? ByteTag.ONE : ByteTag.ZERO);
        }
        tag.put(PUBLICS, lt);

        return tag;
    }

    public void fromTag(CompoundTag tag) {
        int len = tag.getInt(LENGTH);
        this.reinit(len);
        this.index = len;

        this.poses = tag.getLongArray(POSITIONS);
        int[] index = {0};
        index[0] = 0;
        ((ListTag) tag.get(NAMES)).forEach((s)-> {
            this.names[index[0]] = ((StringTag)s).asString();
            index[0]++;
        });

        index[0] = 0;
        ((ListTag) tag.get(DIMENSIONS)).forEach((s)-> {
            this.dims[index[0]] = RegistryKey.of(Registry.DIMENSION, new Identifier(s.asString()));
            index[0]++;
        });

        index[0] = 0;
        ((ListTag) tag.get(PUBLICS)).forEach((s)-> {
            this.isPublic[index[0]] = s.equals(ByteTag.ONE);
            index[0]++;
        });
    }

    public static PortalListv2 fromTagStatic(CompoundTag tag) {
        PortalListv2 container = new PortalListv2(0);
        container.fromTag(tag);
        return container;
    }

    public int indexOf(BlockPos pos) {
        long p = pos.asLong();
        for (int i = 0; i < this.poses.length; i++) {
            if(this.poses[i] == p) return i;
        }
        return -1;
    }

    public void remove(BlockPos pos) {
        long p = pos.asLong();
        for (int i = 0; i < this.poses.length; i++) {
            if(this.poses[i] == p) {
                this.remove(i);
                return;
            }
        }
    }

    public void remove(int index) {
        this.locker(()-> {
            System.arraycopy(this.names,    index+1, this.names,    index, this.index-(index+1));
            System.arraycopy(this.poses,    index+1, this.poses,    index, this.index-(index+1));
            System.arraycopy(this.dims,     index+1, this.dims,     index, this.index-(index+1));
            System.arraycopy(this.isPublic, index+1, this.isPublic, index, this.index-(index+1));
        });
        this.revokeLatest();
        this.trim();
    }

    public void modifyEntry(int index, Consumer<PortalListEntry> entry) {
        if (index >= this.index || index < 0) {
            throw new IllegalArgumentException("index out of range");
        }
        PortalListEntry e = this.get(index);
        entry.accept(e);
        this.locker(()-> {
            this.poses[index] = e.pos.asLong();
            this.names[index] = e.name;
            this.dims[index] = e.dim;
            this.isPublic[index] = e.isPublic;
        });
    }

    public int length() {
        return this.index;
    }

    public PortalListv2 getPublics() {
        PortalListv2 pl = new PortalListv2(this.length());
        this.forEach((entry)-> {if(entry.isPublic) pl.add(entry);});
        return pl;
    }

}
