package com.biom4st3r.netherportals;

import java.util.Arrays;
import java.util.function.Consumer;

import net.minecraft.nbt.ByteTag;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;

public class PortalListv3 extends MultiList<com.biom4st3r.netherportals.PortalListv3.PortalListEntry> {
    public static class PortalListEntry {
        public BlockPos.Mutable pos;
        public String name;
        public RegistryKey<World> dim;
        public boolean isPublic;
        public PortalListEntry(String name, long pos, RegistryKey<World> dim, boolean isPublic) {
            this.reassign(name, new BlockPos.Mutable(), dim, isPublic);
            this.pos.set(pos);
        }

        public PortalListEntry() {
            super();
        }

        public PortalListEntry reassign(String name, BlockPos pos, RegistryKey<World> dim, boolean isPublic) {
            this.pos = pos.mutableCopy();
            this.name = name;
            this.dim = dim;
            this.isPublic = isPublic;
            return this;
        }

        public PortalListEntry reassign(String name, long pos, RegistryKey<World> dim, boolean isPublic) {
            this.pos.set(pos);
            this.name = name;
            this.dim = dim;
            this.isPublic = isPublic;
            return this;
        }
    }

    public void add(String name, long pos, RegistryKey<World> dim, boolean isPublic) {
        this.checkSize();
        this.borrow(()-> {
            this.getCollection(0)[this.length()] = name;
            this.getCollection(1)[this.length()] = pos;
            this.getCollection(2)[this.length()] = dim;
            this.getCollection(3)[this.length()] = isPublic;
        });
        this.index++;
    }

    @Override
    protected Class<?>[] getTypes() {
        return new Class<?>[] {String.class,long.class,RegistryKey.class,boolean.class};
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public void forEach(Consumer<PortalListEntry> consumer) {
        PortalListEntry instance = new PortalListEntry("", 0, null, false);
        this.borrow(()-> {
            for(int i = 0; i < this.length(); i++) {
                instance.reassign((String)this.getCollection(0)[i], (long)this.getCollection(1)[i], (RegistryKey<World>)this.getCollection(2)[i], (boolean)this.getCollection(3)[i]);
                consumer.accept(instance);
            }
        });
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public PortalListv3 getCopy() {
        PortalListv3 list = new PortalListv3();
        list.collections = Arrays.copyOf(this.collections, this.collections.length);
        list.index = this.index;
        list.clone = this.clone+1;
        return list;
    }

    @Override
    @SuppressWarnings({"rawtypes"})
    public CompoundTag toTag(CompoundTag tag) {
        this.trim();
        tag.putInt(LENGTH, this.index);

        tag.putLongArray(POSITIONS, (long[])(Object)Arrays.copyOf(this.getCollection(1), this.index));
        ListTag lt = new ListTag();
        for (int i = 0; i < this.index; i++) {
            lt.add(StringTag.of((String) this.getCollection(0)[i]));
        }
        tag.put(NAMES, lt);

        lt = new ListTag();
        for (int i = 0; i < this.index; i++) {
            lt.add(StringTag.of(((RegistryKey)this.getCollection(2)[i]).getValue().toString()));
        }
        tag.put(DIMENSIONS, lt);

        lt = new ListTag();
        for (int i = 0; i < this.index; i++) {
            lt.add((boolean)this.getCollection(3)[i] ? ByteTag.ONE : ByteTag.ZERO);
        }
        tag.put(PUBLICS, lt);

        return tag;
    }

    @Override
    public void fromTag(CompoundTag tag) {
        int len = tag.getInt(LENGTH);
        this.reinit(len);
        this.index = len;

        this.collections[1] = tag.getLongArray(POSITIONS);
        int[] index = {0};
        index[0] = 0;
        ((ListTag) tag.get(NAMES)).forEach((s)-> {
            this.getCollection(0)[index[0]] = ((StringTag)s).asString();
            index[0]++;
        });

        index[0] = 0;
        ((ListTag) tag.get(DIMENSIONS)).forEach((s)-> {
            this.getCollection(2)[index[0]] = RegistryKey.of(Registry.DIMENSION, new Identifier(s.asString()));
            index[0]++;
        });

        index[0] = 0;
        ((ListTag) tag.get(PUBLICS)).forEach((s)-> {
            this.getCollection(3)[index[0]] = s.equals(ByteTag.ONE);
            index[0]++;
        });
        
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public PortalListEntry get(int i) {        
        if (i >= index) {
            return null;
        }
        return new PortalListEntry((String)this.getCollection(0)[i], (long)this.getCollection(1)[i], (RegistryKey<World>)this.getCollection(2)[i], (boolean)this.getCollection(3)[i]);
    }

    @Override
    public void modifyEntry(int index, Consumer<PortalListEntry> entry) {
        if (index >= this.index || index < 0) {
            throw new IllegalArgumentException("index out of range");
        }
        PortalListEntry e = this.get(index);
        entry.accept(e);
        this.borrow(()-> {
            this.getCollection(0)[index] = e.name;
            this.getCollection(1)[index] = e.pos.asLong();
            this.getCollection(2)[index] = e.dim;
            this.getCollection(3)[index] = e.isPublic;
        });
    }
}
