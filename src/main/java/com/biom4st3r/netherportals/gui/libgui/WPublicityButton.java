package com.biom4st3r.netherportals.gui.libgui;

import io.github.cottonmc.cotton.gui.client.ScreenDrawing;
import net.minecraft.client.util.math.MatrixStack;

/**
 * WPublicityButton
 */
public class WPublicityButton extends WConsumerButton<WPublicityButton> {

    public boolean status;
    public WPublicityButton(boolean status)
    {
        this.status = status;
    }

    public float toCoord(int pos, int imageDim)
    {
        return (float)pos/(float)imageDim;
    }

    @Override
    public void paint(MatrixStack matrices, int x, int y, int mouseX, int mouseY) {
        if(this.status)
        {
            ScreenDrawing.texturedRect(x, y, this.getWidth(), this.getHeight(), BirdBathController.BB_TEXTURES, toCoord(256-24, 256), 0, 1, toCoord(24, 256), 0xFF_FFFFFF);
        }
        else
        {
            ScreenDrawing.texturedRect(x, y, this.getWidth(), this.getHeight(), BirdBathController.BB_TEXTURES, toCoord(256-48, 256), 0, toCoord(256-24, 256), toCoord(24, 256), 0xFF_FFFFFF);
        }
    }
}