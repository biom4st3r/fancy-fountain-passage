package com.biom4st3r.netherportals.gui.libgui;

import io.github.cottonmc.cotton.gui.client.ScreenDrawing;
import io.github.cottonmc.cotton.gui.widget.WButton;
import io.github.cottonmc.cotton.gui.widget.data.HorizontalAlignment;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;

/**
 * WPaintedButton
 */
public class WPaintedButton extends WButton {

    public WPaintedButton(Text name,String location,int index)
    {
        super(name);
        this.location = location;
        this.index = index;
    }
    String location;
    int index;

    public float toCoord(int pos, int imageDim)
    {
        return (float)pos/(float)imageDim;
    }

    @Override
    public void paint(MatrixStack matrices,int x, int y, int mouseX, int mouseY) {
        boolean hovered = (mouseX >=0 && mouseY >=0 && mouseX < getWidth() && mouseY < getHeight());
        ScreenDrawing.drawStringWithShadow(matrices,getLabel().getString(), HorizontalAlignment.CENTER, x,y-1, this.getWidth(), 0x49abba);
        ScreenDrawing.drawStringWithShadow(matrices,location, HorizontalAlignment.CENTER, x, y+11, this.getWidth(), 0x3c7b85);
        if( hovered )
        {
            ScreenDrawing.texturedRect(x, y, this.getWidth(), this.getHeight(), BirdBathController.BB_TEXTURES, 0, toCoord(209, 256), toCoord(115,256), toCoord(232,256), 0xFF_FFFFFF);
        }
    }

    
}