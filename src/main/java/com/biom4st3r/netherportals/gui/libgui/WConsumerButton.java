package com.biom4st3r.netherportals.gui.libgui;

import java.util.function.Consumer;

import io.github.cottonmc.cotton.gui.widget.WWidget;

/**
 * WSupplierButton
 */
public abstract class WConsumerButton <T extends WConsumerButton<T>> extends WWidget {

    protected Consumer<T> onClick = (button)->{};

    public void setOnClick(Consumer<T> consumer)
    {
        onClick = consumer;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public void onClick(int x, int y, int button) {
        this.onClick.accept((T)this);
    }

}