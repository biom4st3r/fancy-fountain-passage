package com.biom4st3r.netherportals.gui.libgui;

import io.github.cottonmc.cotton.gui.client.ScreenDrawing;
import net.minecraft.client.util.math.MatrixStack;

/**
 * WArrowButton
 */
public class WArrowButton extends WConsumerButton<WArrowButton> {

    public enum Orientation
    {
        Forward,
        Backward
    }

    protected boolean visible;
    Orientation orientation;
    public WArrowButton(Orientation ori) {
        this.orientation = ori;
    }

    @Override
    public void onClick(int x, int y, int button) {
        if(visible) super.onClick(x, y, button);
    }

    public float toCoord(int pos, int imageDim)
    {
        return (float)pos/(float)imageDim;
    }
    
    @Override
    public void paint(MatrixStack matrices, int x, int y, int mouseX, int mouseY) {
        boolean hovered = (mouseX>=0 && mouseY>=0 && mouseX<getWidth() && mouseY<getHeight());
        if(!visible) return;
        switch (this.orientation) {
            case Forward:
                if(!hovered)
                {
                    ScreenDrawing.texturedRect(x, y, this.getWidth(), this.getHeight(), BirdBathController.WIDGETS_LOCATION, toCoord(1, 256), toCoord(208, 256), toCoord(12, 256), toCoord(208+17, 256), 0xFF_FFFFFF);
                }
                else
                {
                    ScreenDrawing.texturedRect(x, y, this.getWidth(), this.getHeight(), BirdBathController.WIDGETS_LOCATION, toCoord(1, 256), toCoord(226, 256), toCoord(12, 256), toCoord(226+17, 256), 0xFF_FFFFFF);
                }
                break;
            case Backward:
                if(!hovered)
                {
                    ScreenDrawing.texturedRect(x, y, this.getWidth(), this.getHeight(), BirdBathController.WIDGETS_LOCATION, toCoord(15, 256), toCoord(208, 256), toCoord(15+11, 256), toCoord(208+17, 256), 0xFF_FFFFFF);
                }
                else
                {
                    ScreenDrawing.texturedRect(x, y, this.getWidth(), this.getHeight(), BirdBathController.WIDGETS_LOCATION, toCoord(15, 256), toCoord(226, 256), toCoord(15+11, 256), toCoord(226+17, 256), 0xFF_FFFFFF);
                }
                break;
        }
    }

    /**
     * @return the visible
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * @param visible the visible to set
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

}