package com.biom4st3r.netherportals.gui.libgui;

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;
import net.minecraft.entity.player.PlayerEntity;

/**
 * BirdBathGui2
 */
public class BirdBathGui extends CottonInventoryScreen<BirdBathController> {

    public BirdBathGui(BirdBathController container, PlayerEntity player) {
        super(container, player);
    }

}