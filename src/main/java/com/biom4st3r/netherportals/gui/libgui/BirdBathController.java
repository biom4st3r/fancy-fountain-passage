package com.biom4st3r.netherportals.gui.libgui;

import com.biom4st3r.netherportals.BioLogger;
import com.biom4st3r.netherportals.ComponentReg;
import com.biom4st3r.netherportals.ModInit;
import com.biom4st3r.netherportals.PortalListv2;
import com.biom4st3r.netherportals.components.IPortalComponent;
import com.biom4st3r.netherportals.gui.libgui.WArrowButton.Orientation;

import io.github.cottonmc.cotton.gui.SyncedGuiDescription;
import io.github.cottonmc.cotton.gui.client.BackgroundPainter;
import io.github.cottonmc.cotton.gui.client.ScreenDrawing;
import io.github.cottonmc.cotton.gui.widget.WButton;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WLabel;
import io.github.cottonmc.cotton.gui.widget.WPanel;
import io.github.cottonmc.cotton.gui.widget.WPlainPanel;
import io.github.cottonmc.cotton.gui.widget.WTextField;
import io.github.cottonmc.cotton.gui.widget.WWidget;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;

/**
 * BirdBathController
 */
public class BirdBathController extends SyncedGuiDescription  {

    public static final Identifier BB_TEXTURES = new Identifier(ModInit.MODID,"textures/gui/birdbath.png");
    public static final Identifier CHISELED_QUARTZ_TOP = new Identifier("minecraft", "textures/block/chiseled_quartz_block_top.png");
    public static final Identifier PURPUR_PILLAR_TOP = new Identifier("minecraft", "textures/block/purpur_pillar_top.png");
    public static final Identifier WIDGETS_LOCATION = new Identifier("textures/gui/recipe_book.png");
    public static Identifier BG_Texture;
    private static BioLogger logger = new BioLogger("BirdBathController");
    IPortalComponent component;
    PortalListv2 plist;
    String portalListOwner;
    ScreenHandlerContext blockcontext;
    int page = 0;
    int maxPages = 666;
    private final int rows = 6;
    private static PortalListv2 altPortalList = null;
    private static boolean receivedNewPortalList = false;
    public static PortalListv2 getaltPortalList()
    {
        return altPortalList;
    }

    public static void setAltPortalList(PortalListv2 pl)
    {
        // TODO Need to reset portalListOwner when a name is searched
        altPortalList = pl;
        receivedNewPortalList = true;
    }

    WArrowButton[] arrowButtons = new WArrowButton[2];
    WPaintedButton[] teleportButtons = new WPaintedButton[rows];
    WPublicityButton[] publicityButtons = new WPublicityButton[rows];
    WButton add_remove_button;
    WLabel pageLabel;

    public BirdBathController(int syncId, PlayerInventory playerInventory,ScreenHandlerContext context) {
        super(ModInit.birdBathScreenType, syncId, playerInventory, null,null);
        this.playerInventory = playerInventory;
        portalListOwner = playerInventory.player.getEntityName();
        this.blockcontext = context;
        BG_Texture = getTexture();
        component = ComponentReg.TYPE.get(playerInventory.player);
        plist = component.getPortalList();
        if(playerInventory.player.world.isClient)
        {
            logger.debug("client");
        }
        else
        {
            logger.debug("Server");
        }
        WGridPanel root = new WGridPanel(1) {
            @Override
            protected void expandToFit(WWidget w) {
                
            }
        };
        if(playerInventory.player.world.isClient)
        {
            this.setRootPanel(root);
            root.setSize(200,200);
    
            WPlainPanel animatedPanel = new WPlainPanel();
            animatedPanel.setBackgroundPainter((left,top,panel)->
            {
                long current = System.currentTimeMillis()/50;
                ScreenDrawing.texturedRect(left, top, panel.getWidth(), panel.getHeight(), new Identifier("minecraft", "textures/block/water_still.png"), 0, toCoord((int)(current%32)*16, 512), 1, toCoord(((int)(current%32)*16)+16,512), 0x32585E);
            });
            root.add(animatedPanel, 25, 25,150,150);
            
            WTextField nameField = new WResponsiveTextField(newText(playerInventory.player.getEntityName()));
            nameField.setMaxLength(16);
            int width = (int)(16*5F)+12; // 12 for spacing
            root.add(nameField, (root.getWidth()/2)-width/2,3, width, 15);
            nameField.setChangedListener((string)->
            {
                this.portalListOwner = string;
                component.searchPlayer(string);
            });
            if(blockcontext.run((world,pos)->world.getBlockState(pos).getBlock() == ModInit.purpurPortalBlock, false))
            {
                pageLabel = new WLabel("");
                pageLabel.setColor(0xFF_FFFFFF, 0xFF_FFFFFF);
                root.add(pageLabel, (root.getWidth()/2)-12, root.getHeight()-17);

                arrowButtons[0] = new WArrowButton(Orientation.Backward);
                arrowButtons[0].setVisible(page > 0);
                arrowButtons[0].setOnClick((arrowbutton)->
                {
                    --page;
                    refreshPage(root);
                });
                arrowButtons[1] = new WArrowButton(Orientation.Forward);
                arrowButtons[1].setVisible(maxPages> page);
                arrowButtons[1].setOnClick((arrowButton)->
                {
                    ++page;
                    refreshPage(root);
                });
                root.add(arrowButtons[0], (root.getWidth()/2)-31, root.getHeight()-20);
                root.add(arrowButtons[1], (root.getWidth()/2)+20, root.getHeight()-20);
                arrowButtons[0].setSize(11, 15);
                arrowButtons[1].setSize(11, 15);
            }
            refreshPage(root);

        }
        // root.add(new WButton(new LiteralText("ffffffffffffff")),0,0);
        root.validate(this);
    }

    @Override
    public void close(PlayerEntity player) {
        super.close(player);
        altPortalList = null;
    }

    private void generateAdd_Remove_Button(WGridPanel root) {
        if(add_remove_button != null) root.remove(add_remove_button);
        if(altPortalList == null)
        {
            Text text;
            BlockPos current = blockcontext.run((world,pos)->pos, BlockPos.ORIGIN);
            if(plist.indexOf(current) != -1)
            {
                text = new TranslatableText("dialog.biom4st3rportal.remove_portal");
                add_remove_button = new WButton(text);
                add_remove_button.setOnClick(()->
                {
                    // portalListOwner = this.playerInventory.player.getEntityName();
                    component.removeEntry(blockcontext.run((world,pos)->pos, BlockPos.ORIGIN));
                    refreshPage(root);
                });
            }
            else
            {
                // System.out.println("helo");
                text = new TranslatableText("dialog.biom4st3rportal.add_portal"); 
                // logger.debug("%s", text.getString());
                add_remove_button = new WButton(text);
                add_remove_button.setOnClick(()->
                {
                    // portalListOwner = this.playerInventory.player.getEntityName();
                    WGridPanel panel = new WGridPanel(1);
                    panel.setBackgroundPainter(BackgroundPainter.VANILLA);
                    WTextField field = new WResponsiveTextField(new TranslatableText("dialog.biom4st3rportal.locationname"));
                    field.setMaxLength(23);
                    field.setChangedListener((string)->
                    {
                        logger.debug("Sub Window ResponsiveTextField");
                        component.registerNewLocation(field.getText(), current);
                        root.remove(panel);
                        refreshPage(root);
                    });
                    panel.add(field, 0, 0, 100, 30);
                    field.setHost(this);
                    Text submitText = new TranslatableText("dialog.biom4st3rportal.submitbutton");
                    WButton submit = new WButton(submitText);
                    submit.setOnClick(()->
                    {
                        component.registerNewLocation(field.getText(), current);
                        logger.debug("setOnClick(%s)", field.getText());
                        root.remove(panel);
                        refreshPage(root);
                    });
                    panel.add(submit, 0, 30,submitText.getString().length()*7,20);
                    root.add(panel, (root.getWidth()/2)-50, (root.getHeight()/2)-25, 100, 50);
                    panel.createPeers(this);
                    field.requestFocus();
                });

            }
            //~5.2 pixels per char + 12 for side padding
            int length = text.getString().length()*7;
            root.add(add_remove_button, (root.getWidth()/2)-length/2, 200,length,20);
        }
    }

    public void refreshPage(WGridPanel root)
    {
        plist = component.getPortalList();
        maxPages = ((plist.length()-1)/rows);
        if(pageLabel != null)
        pageLabel.setText(newText(String.format("%s / %s", this.page+1,this.maxPages+1)));
        if(arrowButtons[0] != null)
        {
            this.arrowButtons[0].setVisible(page > 0);
            this.arrowButtons[1].setVisible(maxPages>page);
        }

        generateTeleportButtons(root);
        generatePublicityButtons(root);
        generateAdd_Remove_Button(root);
    }

    public static void removeAll(WPanel root,WWidget... widgets)
    {
        for(WWidget w : widgets)
        {
            root.remove(w);
        }
    }

    private int getTrueIndex(int i) {
        return (rows * this.page) + i;
    }

    private void generateTeleportButtons(WGridPanel root)
    {
        removeAll(root, teleportButtons);
        for(int i = 0; i < rows && getTrueIndex(i) < plist.length(); i++)
        {
            int trueIndex = getTrueIndex(i);
            try 
            {
                // I really didn't want to try catch, because i could just math it, but ¯\_(ツ)_/¯
                teleportButtons[i] = new WPaintedButton(newText(plist.get(trueIndex).name), plist.get(trueIndex).pos.toShortString(),i);
                root.add(teleportButtons[i], (root.getWidth()/2)-(114/2), 28+((25*i)), 114, 24);
                // int index = teleportButtons[i].index;
                teleportButtons[i].setOnClick(()->
                {
                    component.requestTeleport(trueIndex, portalListOwner, blockcontext.run((world,pos)->pos, BlockPos.ORIGIN));
                });
            }
            catch(IndexOutOfBoundsException e) {break;}
        }

    }
    public Text newText(String str) {return new LiteralText(str);}

    private void generatePublicityButtons(WGridPanel root) {
        removeAll(root, publicityButtons);
        for(int i = 0; i < rows && getaltPortalList() == null && getTrueIndex(i) < plist.length(); i++)
        {
            try
            {
                int trueIndex = this.getTrueIndex(i);
                publicityButtons[i] = new WPublicityButton(plist.get(trueIndex).isPublic);
                root.add(publicityButtons[i], 30, 28+(25*i)+2);
                publicityButtons[i].setSize(12,12);
                // int index = i;
                publicityButtons[i].setOnClick((button)->
                {
                    button.status = !button.status;
                    component.changePublicity(trueIndex, button.status);
                });
            }
            catch(IndexOutOfBoundsException e) {break;}
        }
    }

    @Override
    public boolean canUse(PlayerEntity entity) {
        return canUse(blockcontext, entity, ModInit.purpurPortalBlock) || canUse(blockcontext, entity, ModInit.quartzPortalBlock);
    }

    public BackgroundPainter basicPainter = (left,top,panel)->
    {
        if(receivedNewPortalList)
        {
            this.refreshPage((WGridPanel) this.getRootPanel());
            receivedNewPortalList = false;
        }
        ScreenDrawing.texturedRect(left, top, panel.getWidth(), panel.getHeight(), BG_Texture,0,0,1,1, 0xFF_FFFFFF);
    };

    private Identifier getTexture()
    {
        Block block = blockcontext.run((world,pos)->world.getBlockState(pos).getBlock(),Blocks.AIR);
        if(block == ModInit.purpurPortalBlock) return PURPUR_PILLAR_TOP;
        else if(block == ModInit.quartzPortalBlock) return CHISELED_QUARTZ_TOP;
        return new Identifier("");
    }

    public float toCoord(int pos, int imageDim)
    {
        return (float)pos/(float)imageDim;
    }
    
    
    @Override
    @Environment(EnvType.CLIENT)
    public void addPainters() {
        this.rootPanel.setBackgroundPainter(basicPainter);
    }
}