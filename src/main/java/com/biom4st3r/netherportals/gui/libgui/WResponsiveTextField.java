package com.biom4st3r.netherportals.gui.libgui;

import org.lwjgl.glfw.GLFW;

import io.github.cottonmc.cotton.gui.widget.WTextField;
import net.minecraft.text.Text;

/**
 * UpdatingTextFeild
 */
public class WResponsiveTextField extends WTextField {
    @Override
    public void onKeyPressed(int ch, int key, int modifiers) {
        super.onKeyPressed(ch, key, modifiers);
        if (ch == GLFW.GLFW_KEY_ENTER || ch == GLFW.GLFW_KEY_KP_ENTER) {
            // this.setText(this.getText());
            this.onChanged.accept(this.getText());
            this.releaseFocus();
        }
    }

    @Override
    public void onFocusLost() {
    }

    /**
     * @param suggestion
     */
    public WResponsiveTextField(Text suggestion) {
        super(suggestion);
    }

    /**
     * 
     */
    public WResponsiveTextField() {
    }
    
}