package com.biom4st3r.netherportals;

import com.biom4st3r.netherportals.gui.libgui.BirdBathController;
import com.biom4st3r.netherportals.gui.libgui.BirdBathGui;
import com.biom4st3r.netherportals.registries.Packets;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry;

@Environment(EnvType.CLIENT)
public class NetherPortalClientMod implements ClientModInitializer
{
    
    @Override
    public void onInitializeClient() {
        Packets.Client.init();
        ScreenRegistry.register(ModInit.birdBathScreenType, (ScreenRegistry.Factory<BirdBathController,BirdBathGui>)(gui, inventory, title)->
        {
            return new BirdBathGui(gui, inventory.player);
        });


    }
}