package com.biom4st3r.netherportals.blocks;

import java.util.Random;

import com.biom4st3r.netherportals.gui.libgui.BirdBathController;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.ShapeContext;
import net.minecraft.block.Waterloggable;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.state.StateManager.Builder;
import net.minecraft.state.property.Properties;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

public class BirdBathBlock extends Block implements Waterloggable {

    public BirdBathBlock() {
        // SeaPickleBlock
        super(FabricBlockSettings.copy(Blocks.PURPUR_BLOCK).nonOpaque());
        this.setDefaultState(this.stateManager.getDefaultState().with(Properties.WATERLOGGED, false));
    }

    @Override
    protected void appendProperties(Builder<Block, BlockState> builder) {
        builder.add(Properties.WATERLOGGED);
    }

    public static final VoxelShape SHAPE = VoxelShapes.union(
        Block.createCuboidShape(1, 0, 1, 15, 1, 15),
        Block.createCuboidShape(4, 1, 4, 12, 3, 12),
        Block.createCuboidShape(6, 1, 6, 10, 11, 10),
        Block.createCuboidShape(5, 11, 5, 11, 13, 11),
        Block.createCuboidShape(1, 13, 1, 15, 14, 15),
        Block.createCuboidShape(2, 14, 2, 14, 16, 14)
        ).simplify();

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        return SHAPE;
    }

    @Override
    @Environment(EnvType.CLIENT)
    public void randomDisplayTick(BlockState state, World world, BlockPos blockPos, Random random) {
        for (int i = 0; i < 5; i++)
            switch (random.nextInt() % 4) {

            case 0:
                world.addParticle(ParticleTypes.PORTAL, blockPos.getX() + random.nextFloat(), blockPos.getY() - 0.2,
                        blockPos.getZ(), 0, random.nextFloat() - 0.5D, 0);
                break;
            case 1:
                world.addParticle(ParticleTypes.PORTAL, blockPos.getX(), blockPos.getY() - 0.2,
                        blockPos.getZ() + random.nextFloat(), 0, random.nextFloat() - 0.5D, 0);
                break;
            case 2:
                world.addParticle(ParticleTypes.PORTAL, blockPos.getX() + 1, blockPos.getY() - 0.2,
                        blockPos.getZ() + random.nextFloat(), 0, random.nextFloat() - 0.5D, 0);
                break;
            case 3:
                world.addParticle(ParticleTypes.PORTAL, blockPos.getX() + random.nextFloat(), blockPos.getY() - 0.2,
                        blockPos.getZ() + 1, 0, random.nextFloat() - 0.5D, 0);
                break;

            }
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos blockPos, PlayerEntity player, Hand hand, BlockHitResult hitResult) {
        player.openHandledScreen(new ExtendedScreenHandlerFactory(){

			@Override
			public Text getDisplayName() { 
                return player.getStackInHand(hand).getName();
			}

			@Override
			public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
				return new BirdBathController(syncId, inv, ScreenHandlerContext.create(player.world, hitResult.getBlockPos()));
			}

			@Override
			public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buf) {
				buf.writeBlockPos(hitResult.getBlockPos());
			}
            
        });

        return ActionResult.SUCCESS;
    }

    @Override
    public boolean isTranslucent(BlockState state, BlockView blockView, BlockPos blockPos) {
        return true;
    }
}