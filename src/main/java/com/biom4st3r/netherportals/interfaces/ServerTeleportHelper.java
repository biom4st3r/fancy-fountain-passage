package com.biom4st3r.netherportals.interfaces;

import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;

public interface ServerTeleportHelper
{
    public void requestTeleport(RegistryKey<World> dt,Vec3d blockPos);

    public boolean hasRequestedTeleport();

    public Vec3d teleportPos();

    public RegistryKey<World> teleportDim();
}