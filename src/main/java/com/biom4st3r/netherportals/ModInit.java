package com.biom4st3r.netherportals;

import java.io.File;

import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.structure.StructurePieceType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;

import com.biom4st3r.netherportals.blocks.BirdBathBlock;
import com.biom4st3r.netherportals.gui.libgui.BirdBathController;
import com.biom4st3r.netherportals.registries.Packets;
import com.biom4st3r.netherportals.structure.PortalPiece;

public class ModInit implements ModInitializer {

    public static File playerDataDir; // Assigned from mixin
    public static final BioLogger logger = new BioLogger("FancyFountain");
    public static final String MODID = "biom4st3rportal";

    public static BirdBathBlock purpurPortalBlock;
    public static BirdBathBlock quartzPortalBlock;
    public static Item quartzpPortal;
    public static Item purpurPortal;

    public static ScreenHandlerType<BirdBathController> birdBathScreenType;

    public static final StructurePieceType PIECE_TYPE = PortalPiece::new;
    // public static final StructureFeature<DefaultFeatureConfig> FEATURE = new PortalFeature(DefaultFeatureConfig.CODEC);
    // public static final ConfiguredStructureFeature<?,?> CONFIGURED = FEATURE.configure(DefaultFeatureConfig.DEFAULT);
    // public static RegistryKey<ConfiguredStructureFeature<?, ?>> key;

	@Override
	public void onInitialize() {
        Packets.Server.init();

        // Registry.register(Registry.STRUCTURE_PIECE, PortalPiece.id, PIECE_TYPE);
        // FabricStructureBuilder.create(PortalPiece.id, FEATURE)
        //     .step(GenerationStep.Feature.SURFACE_STRUCTURES)
        //     .defaultConfig(32, 8, 0xDEADBEEF)
        //     .adjustsSurface()
        //     .register();
        // key = RegistryKey.of(Registry.CONFIGURED_STRUCTURE_FEATURE_WORLDGEN, PortalPiece.id);
        // BuiltinRegistries.add(BuiltinRegistries.CONFIGURED_STRUCTURE_FEATURE, key.getValue(), CONFIGURED);
        // BiomeModifications.addStructure(BiomeSelectors.foundInTheEnd(), key);

        purpurPortalBlock = Registry.register(Registry.BLOCK, new Identifier(MODID, "portal"), new BirdBathBlock());
        purpurPortal = Registry.register(Registry.ITEM,new Identifier(MODID, "portal"), new BlockItem(purpurPortalBlock, new Item.Settings().group(ItemGroup.MISC)));

        quartzPortalBlock = Registry.register(Registry.BLOCK, new Identifier(MODID, "quartz_portal"), new BirdBathBlock());
        quartzpPortal = Registry.register(Registry.ITEM,new Identifier(MODID, "quartz_portal"), new BlockItem(quartzPortalBlock, new Item.Settings().group(ItemGroup.MISC)));

        birdBathScreenType = ScreenHandlerRegistry.registerExtended(new Identifier(MODID, "bbbgui"), (syncid,inv,pbb)->
        { 
            return new BirdBathController(syncid, inv,ScreenHandlerContext.create(inv.player.world, pbb.readBlockPos()));
        });
    }
}
