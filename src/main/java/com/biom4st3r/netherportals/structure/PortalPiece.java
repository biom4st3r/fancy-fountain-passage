package com.biom4st3r.netherportals.structure;

import java.util.List;
import java.util.Random;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.structure.SimpleStructurePiece;
import net.minecraft.structure.Structure;
import net.minecraft.structure.StructureManager;
import net.minecraft.structure.StructurePiece;
import net.minecraft.structure.StructurePlacementData;
import net.minecraft.structure.processor.BlockIgnoreStructureProcessor;
import net.minecraft.util.BlockRotation;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockBox;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.ServerWorldAccess;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.StructureAccessor;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.gen.feature.DefaultFeatureConfig;

import com.biom4st3r.netherportals.ModInit;

public class PortalPiece extends SimpleStructurePiece {

    public static final Identifier id = new Identifier(ModInit.MODID,"birdbathpiece");
        
    public static void addParts(StructureManager structureManager, BlockPos pos, BlockRotation rotation, List<StructurePiece> children, Random random, DefaultFeatureConfig config) {
        PortalPiece piece = new PortalPiece(structureManager);
        piece.pos = pos;
        children.add(piece);
    }


    public PortalPiece(StructureManager manager) {
        super(ModInit.PIECE_TYPE, 0);
        init(manager);
    }

    private void init(StructureManager manager) {
        Structure struct = manager.getStructureOrBlank(id);
        StructurePlacementData data = new StructurePlacementData().addProcessor(BlockIgnoreStructureProcessor.IGNORE_AIR_AND_STRUCTURE_BLOCKS);
        this.setStructureData(struct, this.pos, data);
    }

    public PortalPiece(StructureManager manager, CompoundTag compoundTag) {
        super(ModInit.PIECE_TYPE, compoundTag);
        init(manager);
    }

    @Override
    public boolean generate(StructureWorldAccess structureWorldAccess, StructureAccessor structureAccessor, ChunkGenerator chunkGenerator, Random random, BlockBox boundingBox, ChunkPos chunkPos,
            BlockPos blockPos) {
        return super.generate(structureWorldAccess, structureAccessor, chunkGenerator, random, boundingBox, chunkPos, blockPos);
    }

    @Override
    protected void handleMetadata(String metadata, BlockPos pos, ServerWorldAccess serverWorldAccess, Random random, BlockBox boundingBox) {
        
    }
    
}
