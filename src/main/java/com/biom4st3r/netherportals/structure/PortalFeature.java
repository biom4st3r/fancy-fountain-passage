package com.biom4st3r.netherportals.structure;

import java.util.Map;

import com.biom4st3r.netherportals.reflection.FieldRef.FieldHandler;
import com.mojang.serialization.Codec;

import net.minecraft.structure.StructureManager;
import net.minecraft.structure.StructureStart;
import net.minecraft.util.BlockRotation;
import net.minecraft.util.math.BlockBox;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.DynamicRegistryManager;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStep;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.gen.feature.DefaultFeatureConfig;
import net.minecraft.world.gen.feature.StructureFeature;

public class PortalFeature extends StructureFeature<DefaultFeatureConfig> {

    static FieldHandler<Map<StructureFeature<?>, GenerationStep.Feature>> handler = FieldHandler.get(StructureFeature.class, null, 1);

    public PortalFeature(Codec<DefaultFeatureConfig> codec) {
        super(codec);
    }

    @Override
    public StructureStartFactory<DefaultFeatureConfig> getStructureStartFactory() {
        return PortalFeature.Start::new;
    }
    // ShipwreckFeature
    public static class Start extends StructureStart<DefaultFeatureConfig> {

        public Start(StructureFeature<DefaultFeatureConfig> feature, int chunkX, int chunkZ, BlockBox box, int references, long seed) {
            super(feature, chunkX, chunkZ, box, references, seed);
        }

        @Override
        public void init(DynamicRegistryManager registryManager, ChunkGenerator chunkGenerator, StructureManager manager, int chunkX, int chunkZ, Biome biome, DefaultFeatureConfig config) {
            BlockRotation rot = BlockRotation.random(this.random);
            BlockPos pos = new BlockPos(chunkX*16, 90, chunkZ*16);
            PortalPiece.addParts(manager, pos, rot, this.children, this.random, config);
            this.setBoundingBoxFromChildren();
        }
    }
    
}
