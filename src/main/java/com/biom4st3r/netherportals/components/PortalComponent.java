package com.biom4st3r.netherportals.components;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.math.BlockPos;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;

import com.biom4st3r.netherportals.ModInit;
import com.biom4st3r.netherportals.PortalListv2;
import com.biom4st3r.netherportals.gui.libgui.BirdBathController;
import com.biom4st3r.netherportals.registries.Packets;
import io.netty.buffer.Unpooled;

/**
 * PortalComponent
 */
public class PortalComponent implements IPortalComponent {

    PlayerEntity pe;
    // PortalList list;
    PortalListv2 portallist;

    public PortalComponent(PlayerEntity pe) {
        this.pe = pe;
        // list = PortalList.getPortalList(pe);
        this.portallist = new PortalListv2(0);
    }

    @Override
    public void readFromNbt(CompoundTag tag) {
        this.fromTag(tag);
    }

    @Override
    public void writeToNbt(CompoundTag tag) {
        this.toTag(tag);
    }

    @Override
    public void fromTag(CompoundTag tag) {
        this.portallist.fromTag(tag);
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
        return this.portallist.toTag(tag);
    }

    @Override
    public PortalListv2 getPortalList() {
        if(BirdBathController.getaltPortalList() != null) return BirdBathController.getaltPortalList(); // TODO
        return portallist;
    }

    private PacketByteBuf newBuff()
    {
        return new PacketByteBuf(Unpooled.buffer());
    }
    
    @Override
    @Environment(EnvType.CLIENT)
    public void registerNewLocation(String name, BlockPos pos) {
        portallist.add(pos, name, pe.world.getRegistryKey(), false);
        PacketByteBuf pbb = newBuff();
        pbb.writeString(name);
        pbb.writeBlockPos(pos);
        ClientPlayNetworking.send(Packets.REGISTER_NEW_PORTAL, pbb);
    }


    @Override
    @Environment(EnvType.CLIENT)
    public void requestTeleport(int index, String owner, BlockPos pos) {
        PacketByteBuf pbb = newBuff();
        pbb.writeInt(index);
        pbb.writeString(owner);
        pbb.writeBlockPos(pos);
        ClientPlayNetworking.send(Packets.REQUEST_TELEPORT, pbb);
    }

    @Override
    @Environment(EnvType.CLIENT)
    public void removeEntry(BlockPos pos) {
        portallist.remove(pos);
        ClientPlayNetworking.send(Packets.REMOVE_ENTRY, newBuff().writeBlockPos(pos));
    }

    @Override
    @Environment(EnvType.CLIENT)
    public void searchPlayer(String name) {
        if(name.length() < 3) {
            ModInit.logger.debug("Name to short to search! %s", name);
            return;
        }
        ClientPlayNetworking.send(Packets.SEARCH_PLAYER, newBuff().writeString(name,16));
    }


    @Override
    @Environment(EnvType.CLIENT)
    public void changePublicity(int index, boolean isPublic) {
        PacketByteBuf pbb = newBuff();
        pbb.writeInt(index);
        pbb.writeBoolean(isPublic);
        ClientPlayNetworking.send(Packets.CHANGE_PUBLIC_STATUS, pbb);
        
    }

    // private int sig;

    // @Override
    // public int getPrevSig() {
    //     return sig;
    // }

    // @Override
    // public void setPrevSig(int i) {
    //     this.sig = i;
    // }
}