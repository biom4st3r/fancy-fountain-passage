package com.biom4st3r.netherportals.components;

import net.minecraft.util.math.BlockPos;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import com.biom4st3r.netherportals.PortalListv2;
import com.biom4st3r.netherportals.PortalListv3;
import dev.onyxstudios.cca.api.v3.component.sync.AutoSyncedComponent;

/**
 * IPortalComponent
 */
public interface IPortalComponent extends AutoSyncedComponent {

    // int getPrevSig();
    // void setPrevSig(int i);

    // @Override
    // default boolean shouldSyncWith(ServerPlayerEntity player) {
    //     if (this.getPrevSig() != this.getPortalList().getSignature()) {
    //         this.setPrevSig(this.getPortalList().getSignature());
    //         return true;
    //     }
    //     return false;
    // }

    
    PortalListv2 getPortalList();

    /**
     * 
     * @param name Name of Teleport Entry
     * @param pos Position of Bird Bath Block
     */
    @Environment(EnvType.CLIENT)
    void registerNewLocation(String name, BlockPos pos);

    /**
     * @param index The index of the Teleport Entry in a {@link PortalListv3}
     * @param owner playerName of the owner of said {@link PortalListv3}
     * @param pos Position of the Bird Bath BLock being used in this menu
     */
    @Environment(EnvType.CLIENT)
    void requestTeleport(int index, String owner, BlockPos pos);
    
    /**
     * @param pos Position of the TeleportEntry. Must be in {@link PortalListv3#positions}
     */
    @Environment(EnvType.CLIENT)
    void removeEntry(BlockPos pos);
    
    @Environment(EnvType.CLIENT)
    void searchPlayer(String name);
    
    /**
     * @param index refers to the index within {@link PortalListv3#isPublic}
     */
    @Environment(EnvType.CLIENT)
    void changePublicity(int index, boolean isPublic);

}