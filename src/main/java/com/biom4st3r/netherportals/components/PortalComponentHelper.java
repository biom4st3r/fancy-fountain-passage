package com.biom4st3r.netherportals.components;

import java.io.File;
import java.util.Optional;
import java.util.UUID;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.integrated.IntegratedServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;

import com.biom4st3r.netherportals.ComponentReg;
import com.biom4st3r.netherportals.ModInit;
import com.biom4st3r.netherportals.PortalListv2;
import com.biom4st3r.netherportals.Requester;
import com.mojang.authlib.GameProfile;

public class PortalComponentHelper {
    public static PortalListv2 fetchOtherPlayerList(String playername, ServerWorld world) {
        // DesertWellFeature
        if(world.getServer().isOnlineMode()) {
            return fetchOnlineModePortalList(playername, world);
        } else {
            ModInit.logger.log("The server appears to be offline. Using offline uuid for search");
            return fetchOfflineModePortalList(playername, world);
        }
    }

    private static PortalListv2 fetchOfflineModePortalList(String playername, ServerWorld world) {
        ServerPlayerEntity entity = world.getServer().getPlayerManager().getPlayer(playername);
        
        if(entity == null) {
            UUID uuid = PlayerEntity.getOfflinePlayerUuid(playername);
            File player = new File(ModInit.playerDataDir, String.format("%s.dat", uuid.toString()));
            if(player.exists() && player.isFile()) {
                GameProfile profile = world.getServer().getUserCache().getByUuid(uuid);
                entity = world.getServer().getPlayerManager().createPlayer(profile);
                world.getServer().getPlayerManager().loadPlayerData(entity);
            }
            else {
                ModInit.logger.debug("UUID lookup failed for %s", playername);
                return new PortalListv2(0);
            }
        }
        return ComponentReg.TYPE.get(entity).getPortalList();
    }

    private static PortalListv2 fetchOnlineModePortalList(String playername, ServerWorld world) {
        ServerPlayerEntity entity = world.getServer().getPlayerManager().getPlayer(playername);
        System.out.println(world.getServer() instanceof IntegratedServer);
        
        if(entity == null) { 
            Optional<UUID> uuid = Requester.getOfflinePlayerUUID(playername);
            File player = new File(ModInit.playerDataDir, String.format("%s.dat", uuid.get().toString()));
            if(uuid.isPresent() && player.exists() && player.isFile()) {

                // CompoundTag tag = null;
                // try {
                //     tag = NbtIo.readCompressed(player);
                // } catch (IOException e) { e.printStackTrace(); }
                // ComponentContainer container = null;
                GameProfile profile = world.getServer().getUserCache().getByUuid(uuid.get());
                // // entity = new ServerPlayerEntity(world.getServer(), world, profile, new ServerPlayerInteractionManager(world));
                entity = world.getServer().getPlayerManager().createPlayer(profile);
                world.getServer().getPlayerManager().loadPlayerData(entity);
                // ComponentContainer container = CardinalEntityInternals.createEntityComponentContainer(entity);
                // container.fromTag(tag);
            }
            else {
                ModInit.logger.debug("UUID lookup failed for %s", playername);
                return new PortalListv2(0);
            }
        }
        else {
            Requester.cacheUUID(entity);
        }
        return ComponentReg.TYPE.get(entity).getPortalList();
    }
}
