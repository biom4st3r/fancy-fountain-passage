package com.biom4st3r.netherportals;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.JsonHelper;

public class Requester {
    
    public static final Map<String, Optional<UUID>> IDS = Maps.newHashMap();
    private static Gson GSON = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(Account.class, new Account.Serializer()).create();
    public static class Account {
        String name;
        UUID id;
        public static class Serializer implements JsonDeserializer<Account> {
 
            @Override
            public Account deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                Account account = new Account();
                account.id = UUID.fromString(uuidFixer(JsonHelper.getString(json.getAsJsonObject(), "id")));
                account.name = JsonHelper.getString(json.getAsJsonObject(), "name");
                return account;
            }
        }
    }
    public static void cacheUUID(PlayerEntity pe) {
        IDS.computeIfAbsent(pe.getEntityName(), (name)->Optional.of(pe.getUuid()));
    }

    private static String uuidFixer(String uuid) {
        StringBuilder builder = new StringBuilder();
        builder.append(uuid.substring(0, 8));
        builder.append('-');
        builder.append(uuid.substring(8,12));
        builder.append('-');
        builder.append(uuid.subSequence(12, 16));
        builder.append('-');
        builder.append(uuid.subSequence(16, 20));
        builder.append('-');
        builder.append(uuid.subSequence(20,uuid.length()));
        return builder.toString();
    }

    public static Optional<UUID> getOfflinePlayerUUID(String name) {
        return IDS.computeIfAbsent(name.toLowerCase(), (n)-> {
            try {
                HttpURLConnection connection = (HttpURLConnection) new URL(String.format("https://api.mojang.com/users/profiles/minecraft/%s?at=%s", n, java.time.Instant.now().getEpochSecond())).openConnection();
                connection.setRequestProperty("accept", "application/json");
                if(connection.getResponseCode() != 200) {
                    return Optional.empty();
                }
                Account account = GSON.fromJson(new InputStreamReader(connection.getInputStream()), Account.class);
                return Optional.of(account.id);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return Optional.empty();
        });
    }

}
