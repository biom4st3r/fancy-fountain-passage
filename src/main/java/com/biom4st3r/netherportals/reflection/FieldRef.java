package com.biom4st3r.netherportals.reflection;

import java.lang.reflect.Field;

public interface FieldRef<R> {
    
    @SuppressWarnings("unchecked")
    default R getOrSet(Object host, R value) {
        try {
            if (value == null) { 
                return (R) getField().get(host);
            } else {
                getField().set(host, value);
                return null;
            }
        }
        catch(Throwable t)
        {
            throw new RuntimeException(t);
        }
    }
    Field getField() throws Throwable;

    
    static <R> FieldRef<R> getFieldGetter(Class<?> target, String name, int index) {
        try
        {
            if (name != null) {
                Field field = target.getDeclaredField(name);
                field.setAccessible(true);
                return () -> field;
            } else {
                Field field = target.getDeclaredFields()[index];
                field.setAccessible(true);
                return () -> field;
            }
        }
        catch(Throwable t)
        {
            throw new RuntimeException(t);
        }
    }

    static <R> FieldRef<R> getFieldSetter(Class<?> target, String name, int index) {
        try
        {
            if (name != null) {
                Field field = target.getDeclaredField(name);
                field.setAccessible(true);
                return () -> field;
            } else {
                Field field = target.getDeclaredFields()[index];
                field.setAccessible(true);
                return () -> field;
            }
        }
        catch(Throwable t)
        {
            throw new RuntimeException(t);
        }
    }
    
    public static interface FieldHandler<T>
    {
        T get(Object o);
        void set(Object o,T t);

        // @SuppressWarnings("unchecked")
        static <T> FieldHandler<T> get(Class<?> target,String name, int index)
        {
            return new FieldHandler<T>() {
                FieldRef<T> get = FieldRef.getFieldGetter(target, name, index);
                FieldRef<T> set = FieldRef.getFieldSetter(target, name, index);

				@Override
				public T get(Object o) {
					return get.getOrSet(o,null);
				}

				@Override
				public void set(Object o,T t) {
					set.getOrSet(o,t);
                }
            };
        }
    }
}

