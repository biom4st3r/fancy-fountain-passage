package com.biom4st3r.netherportals;

import com.biom4st3r.netherportals.components.IPortalComponent;
import com.biom4st3r.netherportals.components.PortalComponent;

import dev.onyxstudios.cca.api.v3.component.ComponentKey;
import dev.onyxstudios.cca.api.v3.component.ComponentRegistry;
import dev.onyxstudios.cca.api.v3.entity.EntityComponentFactoryRegistry;
import dev.onyxstudios.cca.api.v3.entity.EntityComponentInitializer;
import nerdhub.cardinal.components.api.util.RespawnCopyStrategy;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Identifier;

public class ComponentReg implements EntityComponentInitializer {

    public static final ComponentKey<IPortalComponent> TYPE = ComponentRegistry.getOrCreate(new Identifier(ModInit.MODID, "portal_component"), IPortalComponent.class);

    @Override
    public void registerEntityComponentFactories(EntityComponentFactoryRegistry registry) {
        registry.beginRegistration(PlayerEntity.class, TYPE).respawnStrategy(RespawnCopyStrategy.ALWAYS_COPY).end((entity)-> {
            return new PortalComponent(entity);
        });
    }
}
