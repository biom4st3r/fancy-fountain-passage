package com.biom4st3r.netherportals.registries;

import com.biom4st3r.netherportals.ComponentReg;
import com.biom4st3r.netherportals.ModInit;
import com.biom4st3r.netherportals.PortalListv2;
import com.biom4st3r.netherportals.blocks.BirdBathBlock;
import com.biom4st3r.netherportals.components.IPortalComponent;
import com.biom4st3r.netherportals.components.PortalComponentHelper;
import com.biom4st3r.netherportals.gui.libgui.BirdBathController;
import com.biom4st3r.netherportals.interfaces.ServerTeleportHelper;

import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.s2c.play.CustomPayloadS2CPacket;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;

public class Packets
{
    private static final String MODID = ModInit.MODID;

    /**
     * Only used as a response to SEARCH_PLAYER now
     */
    public static final Identifier    SEND_ALL_PORTAL = new Identifier(MODID,"sendportallocations");
    
    //#region Client Packets

    public static final Identifier  REGISTER_NEW_PORTAL = new Identifier(MODID,"registernewportal");
    public static final Identifier     REQUEST_TELEPORT = new Identifier(MODID,"requestteleport");
    public static final Identifier         REMOVE_ENTRY = new Identifier(MODID,"removeentry");
    public static final Identifier        SEARCH_PLAYER = new Identifier(MODID,"searchplayer");
    public static final Identifier CHANGE_PUBLIC_STATUS = new Identifier(MODID,"changelocstat");
        
    public static class Server
    {
        public static void init()
        {
            ServerPlayNetworking.registerGlobalReceiver(Packets.REGISTER_NEW_PORTAL, (server,player,handler,pbb,sender)-> {
                ModInit.logger.debug("packet(REGISTER_NEW_PORTAL)");
                String name = pbb.readString(23);
                ModInit.logger.debug("%s", name);
                BlockPos bpos = pbb.readBlockPos();
                server.execute(() -> {
                    boolean within_range = Math.sqrt(player.squaredDistanceTo(bpos.getX(), bpos.getY(), bpos.getZ())) < 10;
                    boolean is_bird_bath = player.getEntityWorld().getBlockState(bpos).getBlock() instanceof BirdBathBlock;
                    IPortalComponent component = ComponentReg.TYPE.get(player);
                    if (!within_range) {
                        ModInit.logger.error(player.getEntityName()
                                + ": Attempted to register a new Ender Bird Bath > 10 Blocks away. Vanilla Reach is ~5");
                    }
                    if (!is_bird_bath) {
                        ModInit.logger.error(player.getEntityName()
                                + ": Attempted to register a new Ender Bird Bath at location that's not a portal");
                    }
                    component.getPortalList().add(bpos, name, player.world.getRegistryKey(), false);
                    ComponentReg.TYPE.sync(player);
                });
            });

            /**
             * Requirements:
             * Check the used block is a BirdBath
             * Check the player is near by probably 16 blocks
             * If their using an AltPortalList
             *      if their picking a valid index
             * if their in the nether their are trying to glitch in the ceiling at 125 and 126 y
             * the block their teleporting to is a birdbath
             */
            ServerPlayNetworking.registerGlobalReceiver(Packets.REQUEST_TELEPORT, (server,pe,handler,pbb,sender)-> {
                ModInit.logger.debug("packet(REQUEST_TELEPORT)");
                final int indexOfLocEntry = pbb.readInt();
                final String owner = pbb.readString( 16 );
                final BlockPos thisPortal = pbb.readBlockPos();
                final BlockState thisPortalState = pe.getEntityWorld().getBlockState(thisPortal);
                final Block thisBlock = thisPortalState.getBlock();
                server.execute(()->
                {
                    if(!pe.getBlockPos().isWithinDistance(thisPortal, 32))
                    {
                        pe.sendMessage(new TranslatableText("dialog.biom4st3rportal.bathoutofrange").formatted(Formatting.DARK_PURPLE), false);
                        return;
                    }
                    if(!(thisBlock instanceof BirdBathBlock))
                    {
                        pe.sendMessage(new TranslatableText("dialog.biom4st3rportal.notbirdbath").formatted(Formatting.DARK_PURPLE), false);
                        return;
                    }
                    IPortalComponent component = ComponentReg.TYPE.get(pe);
                    PortalListv2 portalList = (!pe.getEntityName().equals(owner)) ? 
                        PortalComponentHelper.fetchOtherPlayerList(owner, (ServerWorld) pe.getEntityWorld()).getPublics()
                        : component.getPortalList();
                    if(indexOfLocEntry > portalList.length()-1)
                    {
                        ModInit.logger.error("%s selected an invalid index. That shouldn't be possible", pe.getEntityName());
                        return;
                    }
                    RegistryKey<World> destDim = portalList.get(indexOfLocEntry).dim;
                    BlockPos destPos = portalList.get(indexOfLocEntry).pos;
                    if ((destPos.getY() == 125 || destPos.getY() == 126) && pe.getEntityWorld().getRegistryKey() == World.NETHER) {
                        pe.sendMessage(new TranslatableText("dialog.biom4st3rportal.attempttoglitchnetherceil").formatted(Formatting.DARK_PURPLE), false);
                        return;
                    }
                    BlockState destState = pe.getServer().getWorld(destDim).getBlockState(destPos);
                    if(!(destState.getBlock() instanceof BirdBathBlock))
                    {
                        if(pe.getEntityName().equals(owner))
                        {
                            portalList.remove(indexOfLocEntry);
                            ComponentReg.TYPE.sync(pe);
                        }
                        pe.closeHandledScreen();
                        pe.sendMessage(new TranslatableText("dialog.biom4st3rportal.missingportal").formatted(Formatting.DARK_PURPLE), false);
                        pe.requestTeleport(thisPortal.getX() + 0.5D, thisPortal.getY() + 1, thisPortal.getZ() + 0.5D); // Teleport to the top of the in use portal
                        return;
                    }
                    if(destDim != pe.world.getRegistryKey())
                    {
                        if(thisBlock == ModInit.purpurPortalBlock) ((ServerTeleportHelper) pe).requestTeleport(destDim, Vec3d.of(destPos));
                        else 
                        {
                            pe.sendMessage(new TranslatableText("dialog.biom4st3rportal.triedcrossdimteleportwithquartz").formatted(Formatting.DARK_PURPLE), false);
                            pe.closeHandledScreen();
                            return;
                        }
                    }
                    else
                    {
                        pe.requestTeleport(destPos.getX() + 0.5d, destPos.getY() + 1, destPos.getZ() + 0.5d);
                    }
                    pe.getEntityWorld().sendEntityStatus(pe, (byte)46);
                    pe.closeHandledScreen();
                });

            });

            ServerPlayNetworking.registerGlobalReceiver(Packets.REMOVE_ENTRY, (server,player,handler,pbb,sender)-> {
                ModInit.logger.debug("packet(REMOVE_ENTRY)");
                BlockPos bp = pbb.readBlockPos();
                server.execute(()->
                {
                    ServerPlayerEntity pe = (ServerPlayerEntity) player;
                    IPortalComponent component = ComponentReg.TYPE.get(pe);
                    PortalListv2 pl = component.getPortalList();
                    pl.remove(bp);
                    ComponentReg.TYPE.sync(pe);
                });
            });

            ServerPlayNetworking.registerGlobalReceiver(Packets.SEARCH_PLAYER, (server,player,handler,pbb,sender)-> {
                ModInit.logger.debug("packet(SEARCH_PLAYER)");
                String playername = pbb.readString(16).trim();
                if(player.getEntityName().equals(playername)) return;
                server.execute(()->
                {
                    PortalListv2 pl = PortalComponentHelper.fetchOtherPlayerList(playername, (ServerWorld) player.getEntityWorld());
                    // boolean searchingForSelf = player.getEntityName().equals(playername);
                    // if (!searchingForSelf) {
                    ModInit.logger.debug("packet(SEARCH_PLAYER)#createSendPublicPortalLocations");
                    ((ServerPlayerEntity) player).networkHandler.sendPacket(createSendPlayerSearchResults(pl));
                    // }
                });
            });

            ServerPlayNetworking.registerGlobalReceiver(Packets.CHANGE_PUBLIC_STATUS, (server,player,handler,pbb,sender)-> {
                ModInit.logger.debug("packet(CHANGE_PUBLIC_STATUS)");
                int index = pbb.readInt();
                boolean isPublic = pbb.readBoolean();
                server.execute(()->
                {
                    IPortalComponent component = ComponentReg.TYPE.get(player);
                    PortalListv2 pl = component.getPortalList();
                    pl.modifyEntry(index, (entry)->entry.isPublic = isPublic);

                    ComponentReg.TYPE.sync(player);
                });
            });
            
        }
        
        private static CustomPayloadS2CPacket createSendPlayerSearchResults(PortalListv2 pl) 
        {
            PacketByteBuf pbb = new PacketByteBuf(Unpooled.buffer());
            pbb.writeCompoundTag(pl.getPublics().toTag(new CompoundTag()));
            return new CustomPayloadS2CPacket(Packets.SEND_ALL_PORTAL, pbb);
        }
    }
    
    @Environment(EnvType.CLIENT)
    public static class Client
    {
        public static void init()
        {
            ClientPlayNetworking.registerGlobalReceiver(Packets.SEND_ALL_PORTAL, (client,handler,pbb,sender)-> {
                PortalListv2 pl = PortalListv2.fromTagStatic(pbb.readCompoundTag());
                BirdBathController.setAltPortalList(pl);
            });
        }
    }
//#endregion
    
}