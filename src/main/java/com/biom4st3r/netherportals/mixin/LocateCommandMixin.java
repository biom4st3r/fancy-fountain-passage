package com.biom4st3r.netherportals.mixin;

import net.minecraft.server.command.LocateCommand;
import net.minecraft.server.command.ServerCommandSource;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
 
@Mixin(LocateCommand.class)
public abstract class LocateCommandMixin {
 
    private LocateCommandMixin() {
        // NO-OP
    }
 
    @Shadow
    private static int execute(ServerCommandSource source, String name) {
        throw new AssertionError();
    }
 
    // @Inject(method = "register", at = @At(value = "RETURN"))
    // private static void onRegister(CommandDispatcher<ServerCommandSource> dispatcher, CallbackInfo info) {
    //     dispatcher.register(literal("locate").requires(source -> source.hasPermissionLevel(2))
    //             .then(literal("BirdBath").executes(ctx -> execute(ctx.getSource(), "birdbat_structure"))));
    // }
}