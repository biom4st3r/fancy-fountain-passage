package com.biom4st3r.netherportals.mixin;

import java.io.File;

import com.biom4st3r.netherportals.ModInit;
import com.mojang.datafixers.DataFixer;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.world.WorldSaveHandler;
import net.minecraft.world.level.storage.LevelStorage;

@Mixin({WorldSaveHandler.class})
public abstract class WorldSaveHandlerMxn {
    
    @Final @Shadow
    private File playerDataDir;
    
    @Inject(
        at = @At("TAIL"), 
        method = "<init>", 
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE)
    private void capturePlayerFileLocation(LevelStorage.Session session, DataFixer dataFixer, CallbackInfo ci) {
        ModInit.playerDataDir = playerDataDir;
    }
}
