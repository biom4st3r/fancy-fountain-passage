package com.biom4st3r.netherportals.mixin;

import com.biom4st3r.netherportals.interfaces.ServerTeleportHelper;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ChunkTicketType;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;

@Mixin(Entity.class)
public abstract class EntityMixin
{

    @Shadow
    public World world;
    
    @Inject(at = @At("TAIL"),method="baseTick")
    public void tickTeleportRequests(CallbackInfo ci)
    {
        if(!this.world.isClient)
        {
            if((Object)this instanceof ServerPlayerEntity)
            {
                ServerTeleportHelper player = (ServerTeleportHelper)this;
                if(player.hasRequestedTeleport())
                {
                    PlayerEntity pe = (PlayerEntity)(Object)this;
                    pe.world.getRegistryKey();
                    ((ServerWorld)world).getChunkManager().addTicket(ChunkTicketType.POST_TELEPORT, new ChunkPos(new BlockPos(player.teleportPos())), 1, pe.getEntityId());
                    ((ServerPlayerEntity)pe).teleport(((ServerPlayerEntity)pe).getServer().getWorld(player.teleportDim()), player.teleportPos().x, player.teleportPos().y+1, player.teleportPos().z, pe.yaw, pe.pitch);
                }
            }
        }
    }
}