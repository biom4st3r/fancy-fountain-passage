package com.biom4st3r.netherportals.mixin;

import com.biom4st3r.netherportals.interfaces.ServerTeleportHelper;
import com.mojang.authlib.GameProfile;

import org.spongepowered.asm.mixin.Mixin;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;

@Mixin(ServerPlayerEntity.class)
public abstract class ServerPlayerEntityMixin extends PlayerEntity implements ServerTeleportHelper {

    public ServerPlayerEntityMixin(World world, BlockPos pos, float yaw, GameProfile profile) {
        super(world, pos, yaw, profile);
    }

    boolean reqTeleport;
    RegistryKey<World> dt;
    Vec3d destbp;

    @Override
    public void requestTeleport(RegistryKey<World> dt, Vec3d blockPos) {
        this.dt = dt;
        this.destbp = blockPos;
        this.reqTeleport = true;
    }

    @Override
    public boolean hasRequestedTeleport() {
        boolean t = reqTeleport;
        reqTeleport = false;
        return t;
    }

    @Override
    public RegistryKey<World> teleportDim() {
        return dt;
    }

    @Override
    public Vec3d teleportPos() {
        return destbp;
    }

}